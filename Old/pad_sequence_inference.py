import torch


def collate_fn_padd(batch):
    '''
    Pads batch of variable length
    '''
    audio=[]
    text = []
    pose = []
    ph_path=[]
    vid_path=[]
    dmm_pth=[]
    for k in range(len(batch)):
        audio.append(batch[k][0])
        text.append(batch[k][1])
        pose.append(batch[k][2])
        dmm_pth.append(batch[k][3])
        ph_path.append(batch[k][4])
        vid_path.append(batch[k][5])
    ## get sequence lengths
    lengths_audio = torch.tensor([ t.shape[-1] for t in audio ])
    lengths_pose = [ t.shape[-1] for t in pose ]
    lengths_pose_t = torch.tensor(lengths_pose)
    ## padd
    audio = [torch.squeeze(t) for t in audio]
    pose = [torch.Tensor(t) for t in pose]
    audio_pad = [torch.nn.functional.pad(seq, (0,torch.max(lengths_audio)-seq.shape[-1]))for seq in audio]
    pose_pad = [torch.nn.functional.pad(seq, (0, torch.max(lengths_pose_t)-seq.shape[-1])) for seq in pose]
    ## compute mask
    audio_pad=torch.stack(audio_pad).to('cuda')
    pose_pad=torch.permute(torch.stack(pose_pad),(0,2,1)).to('cuda')
    mask_audio = (audio_pad!=0).to('cuda')

    mask_pose = (pose_pad != 0)
    mask_pose=mask_pose.sum(dim=-1)/pose_pad.shape[-1]
    mask_pose.to('cuda')
    return (audio_pad,text,pose_pad,mask_audio,mask_pose,lengths_pose,dmm_pth,ph_path,vid_path)


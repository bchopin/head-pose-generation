import os

os.environ['CUDA_LAUNCH_BLOCKING'] = "1"
import argparse
import time
import torch
import gaussian_diffusion as gf
import numpy as np
from torch.utils.data import DataLoader
import Diffusion_model as tr_model
from Dataset_Inference import AudioPoseDataset
from pad_sequence_inference import collate_fn_padd
from datetime import datetime
from scipy.io import loadmat,savemat
import json

def save_result(video_path,phoneme,dmm_path,generated_pose):

    ph_idx = {"AA": 0, "AE": 1, "AH": 2, "AO": 3, "AW": 4, "AY": 5, "B": 6, "CH": 7, "D": 8, "DH": 9, "EH": 10, "ER": 11, "EY": 12, "F": 13, "G": 14, "HH": 15, "IH": 16, "IY": 17, "JH": 18, "K": 19, "L": 20, "M": 21, "N": 22, "NG": 23, "NSN": 24, "OW": 25, "OY": 26, "P": 27, "R": 28, "S": 29, "SH": 30, "SIL": 31, "T": 32, "TH": 33, "UH": 34, "UW": 35, "V": 36, "W": 37, "Y": 38, "Z": 39, "ZH": 40}

    os.system("rm -r results")
    os.system("mkdir results")
    os.system("ffmpeg -i " + video_path[0] + " -ac 2 -f wav " + str(os.path.join('results', 'wav.wav')) + ' -hide_banner -loglevel error')
    os.system("ffmpeg -i " + video_path[0] + " -vf \"select=eq(n\,0)\" -vf scale=256:256 -q:v 3 " + str(os.path.join('results', 'ff.png')))#-vf scale=256:256
    print(video_path[0])
    ph_token=[]
    for ph in phoneme[0]:
        if len(ph)>2:
            ph=ph[:2]
        if ph in ph_idx:
            ph_token.append(ph_idx[ph])
        else:
            pht='SIL'
            ph_token.append(ph_idx[pht])
    with open('results/ph.json', "w") as fp:
        json.dump(ph_token, fp)

    os.system("cp " + dmm_path[0] + " " + str(os.path.join('results', 'real.mat')))

    dmm = loadmat(dmm_path[0])
    coeff = dmm['coeff']
    nb_frames=coeff.shape[0]
    coeff=generated_pose[0,:nb_frames]
    #coeff[:, 224:227]=generated_pose[0,:nb_frames,:3]
    #coeff[:, 254:257]=generated_pose[0,:nb_frames,3:]
    transform=dmm['transform_params']
    #W=transform[0]/256
    #H=transform[1]/256
    #transform[0]=transform[0]/W
    #transform[1]=transform[1]/H
    #transform[3]=transform[3]/W
    #transform[4]=transform[4]/H
    #savemat(
#	os.path.join('results', 'real.mat'),
#        {'coeff': dmm['coeff'], 'transform_params': transform})

    savemat(
        os.path.join('results', 'generated.mat'),
        {'coeff': coeff, 'transform_params': transform})


def Infer(dataloader,model, diffusion, opt):
    model.load_state_dict(torch.load(f'{opt.load_weights}/model_weights'), strict=True)
    model.eval()
    for batch_idx,(audio,text,pose,mask_audio,mask_pose,len_pose,dmm_path,ph_path,vid_path) in enumerate(dataloader):
        start=datetime.now()        
        B, T = pose.shape[:2]
        cur_len = torch.LongTensor([min(T, m_len) for m_len in len_pose])
        print(pose.shape)
        print(mask_pose.shape)
        xf_proj_audio, xf_out_audio = model.encode_audio(audio)
        xf_proj_text, xf_out_text = model.encode_text(text,"cuda")
        xf_proj_pose, xf_out_pose = model.encode_pose(pose[:,0,:],T)
        head_poses = diffusion.p_sample_loop(
            model,
            (B, T, opt.pose_size),
            clip_denoised=False,
            progress=True,
            model_kwargs={'xf_proj_audio': xf_proj_audio, 'xf_out_audio': xf_out_audio,'xf_proj_text':xf_proj_text, 'xf_out_text':xf_out_text,'xf_proj_pose':xf_proj_pose,'xf_out_pose':xf_out_pose, 'length': cur_len})

        torch.cuda.empty_cache()
        head_poses = torch.permute(head_poses, (2, 0, 1))*mask_pose
        head_poses = torch.permute(head_poses, (1,2,0))
        head_poses_np=head_poses.cpu().detach().numpy()
        pose_np = pose.cpu().detach().numpy()

        dist=np.linalg.norm(pose_np-head_poses_np)
        
        stop=datetime.now()
        #print(f'real value: {pose_np[0]}')
        #print(f'fake value: {head_poses_np[0]}')
        #print(f'average error : {dist}')
        #print(f'time : {stop-start}')
        save_result(vid_path, ph_path, dmm_path, head_poses_np)
        break


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-data_dir', type=str, default='/data/stars/share/MEAD_3dmm')
    parser.add_argument('-epochs', type=int, default=25)
    parser.add_argument('-dropout', type=int, default=0.1)
    parser.add_argument('-batch_size', type=int, default=4)
    parser.add_argument('-audio_len', type=int, default=600)
    parser.add_argument('-audio_size', type=int, default=20)
    parser.add_argument('-audio_channels', type=int, default=768)
    parser.add_argument('-audio_rate', type=int, default=48000)
    parser.add_argument('-noise_size',type=int,default=500)
    parser.add_argument('-pose_len', type=int, default=500)
    parser.add_argument('-pose_size', type=int, default=257)
    parser.add_argument('-log_interval',type=int,default=2)
    parser.add_argument('-device', type=str, default='cuda')
    parser.add_argument('-printevery', type=int, default=1)
    parser.add_argument('-lr', type=int, default=0.0001)
    parser.add_argument('-num_worker', type=int, default=0)
    parser.add_argument('-load_weights', required=True)
    parser.add_argument('-diffusion_steps', type=int, default=1000)
    parser.add_argument('-save_every', type=int, default=1)

    opt = parser.parse_args()

    pose_dataset = AudioPoseDataset(root_dir=opt.data_dir,audio_rate=opt.audio_rate)
    dataloader = DataLoader(pose_dataset, batch_size=opt.batch_size,shuffle=True,collate_fn=collate_fn_padd,num_workers=opt.num_worker)

    model = tr_model.MotionTransformer(257, num_frames=opt.audio_len)
    model = model.cuda()

    beta_scheduler = 'linear'
    betas = gf.get_named_beta_schedule(beta_scheduler, opt.diffusion_steps)
    diffusion = gf.GaussianDiffusion(
        betas=betas,
        model_mean_type=gf.ModelMeanType.EPSILON,
        model_var_type=gf.ModelVarType.FIXED_SMALL,
        loss_type=gf.LossType.MSE
    )  # base is epsilon

    Infer(dataloader,model, diffusion, opt)

if __name__ == "__main__":
    main()

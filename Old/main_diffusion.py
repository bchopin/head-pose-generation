import os

os.environ['CUDA_LAUNCH_BLOCKING'] = "1"
import argparse
import time
import torch
import gaussian_diffusion as gf
import numpy as np
from torch.utils.data import DataLoader
import Diffusion_model as tr_model
from Voice_to_pose_dataset_Aencoder_diff import AudioPoseDataset
from pad_sequence_diffusion import collate_fn_padd
from datetime import datetime

def train_model(dataloader,model, diffusion, sampler, opt):
    if opt.load_weights is not None:
        model.load_state_dict(torch.load(f'{opt.load_weights}/model_weights_best'), strict=True)
    model.train()
    mse_criterion = torch.nn.MSELoss(reduction='none')
    for epoch in range(opt.epochs):
        epochstart=datetime.now()
        for batch_idx,(audio,text,pose,mask_audio,mask_pose,len_pose) in enumerate(dataloader):
            #audio, text,pose,mask_audi,mask_text,mask_pose = audio.to(opt.device), text.to(opt.device),pose.to(opt.device),mask_audio.to(opt.device),mask_text.to(opt.device),mask_pose.to(opt.device)
            start=datetime.now()
            x_start = pose
            B, T = x_start.shape[:2]
            t, _ = sampler.sample(B, x_start.device)

            cur_len = torch.LongTensor([min(T, m_len) for m_len in len_pose])

            output = diffusion.training_losses(
                model=model,
                x_start=x_start,
                t=t,
                model_kwargs={"audio": audio,"text":text,"fpose":pose[:,0,:],"length":cur_len}
            )

            real_noise = output['target']
            fake_noise = output['pred']

            torch.optim.Optimizer.zero_grad(opt.optimizer)

            loss = mse_criterion(fake_noise, real_noise).mean(dim=-1)

            current_loss =  (loss * mask_pose).sum() / mask_pose.sum()
            ffloss=mse_criterion(fake_noise[:,0,:], real_noise[:,0,:]).mean(dim=-1)
            current_loss = current_loss +  ffloss.sum()/opt.batch_size
            current_loss.backward()

            # if epoch_since_last_best >= 100:
            #     for g in opt.optimizer.param_groups:
            #         g['lr'] = g['lr'] * 0.1
            #     epoch_since_last_best = 1

            opt.optimizer.step()
            stop=datetime.now()
            dur=stop-start
            # opt.sched.step(current_loss)
            print('Epoch {} [{}/{}] time {}, loss: {:.4f} '.format(
                epoch, batch_idx+1, len(dataloader),
                dur,current_loss.mean().item()))
        if epoch % opt.save_every == 0:
                cwd = os.getcwd()
                dst = '/temp_save_Diff'
                print("saving weights to " + dst + "/...")
                torch.save(model.state_dict(), cwd + f'{dst}/model_weights')
        epochdur=datetime.now()-epochstart
        print(f'epoch time : {epochdur} ')






def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-data_dir', type=str, default='/data/stars/share/MEAD_3dmm')
    parser.add_argument('-epochs', type=int, default=25)
    parser.add_argument('-dropout', type=int, default=0.1)
    parser.add_argument('-batch_size', type=int, default=32)
    parser.add_argument('-audio_len', type=int, default=600)
    parser.add_argument('-audio_size', type=int, default=20)
    parser.add_argument('-audio_channels', type=int, default=768)
    parser.add_argument('-audio_rate', type=int, default=48000)
    parser.add_argument('-noise_size',type=int,default=500)
    parser.add_argument('-pose_len', type=int, default=500)
    parser.add_argument('-pose_size', type=int, default=257)
    parser.add_argument('-log_interval',type=int,default=2)
    parser.add_argument('-device', type=str, default='cuda')
    parser.add_argument('-printevery', type=int, default=1)
    parser.add_argument('-lr', type=int, default=0.0001)
    parser.add_argument('-num_worker', type=int, default=0)
    parser.add_argument('-load_weights')
    parser.add_argument('-diffusion_steps', type=int, default=1000)
    parser.add_argument('-save_every', type=int, default=1)

    opt = parser.parse_args()
    opt.is_test = False
    # if opt.device == 0:
    #     assert torch.cuda.is_available()


    pose_dataset = AudioPoseDataset(root_dir=opt.data_dir,audio_rate=opt.audio_rate)
    dataloader = DataLoader(pose_dataset, batch_size=opt.batch_size,shuffle=True,collate_fn=collate_fn_padd,num_workers=opt.num_worker)

    model = tr_model.MotionTransformer(257, num_frames=opt.audio_len)
    model = model.cuda()

    sampler = 'uniform'
    beta_scheduler = 'linear'
    betas = gf.get_named_beta_schedule(beta_scheduler, opt.diffusion_steps)
    diffusion = gf.GaussianDiffusion(
        betas=betas,
        model_mean_type=gf.ModelMeanType.EPSILON,
        model_var_type=gf.ModelVarType.FIXED_SMALL,
        loss_type=gf.LossType.MSE
    )  # base is epsilon
    sampler = gf.create_named_schedule_sampler(sampler, diffusion)

    opt.optimizer = torch.optim.Adam(model.parameters(), lr=opt.lr)
    opt.sched = torch.optim.lr_scheduler.ReduceLROnPlateau(opt.optimizer, factor=0.5, patience=2, cooldown=10)


    train_model(dataloader,model, diffusion, sampler, opt)

if __name__ == "__main__":
    main()

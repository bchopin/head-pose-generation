
import os
import torch
from torch.utils.data import Dataset, DataLoader
import glob
from moviepy.editor import *
from scipy.io import loadmat
from pysubparser import parser
import numpy as np
import scipy.signal as sps
from WavLM.WavLM import WavLM, WavLMConfig
from p2fa_py3.p2fa import align_from_subs
import json

class AudioPoseDataset(Dataset):
    def __init__(self,root_dir,audio_rate=48000,maxlength=2000,device='cpu'):
        self.root_dir = root_dir
        file_list = glob.glob(self.root_dir + '/videos/**/*.mat',recursive=True)
        self.data=[]
        self.audio_rate=audio_rate
        self.maxlength= maxlength
        self.device=device
        self.UNIT_VALUE =0.033 #0.029931972789115413
        self.MONOPHONE = ['EH2', 'K', 'S', 'L', 'AH0', 'M', 'EY1', 'SH', 'N', 'P', 'OY2', 'T', 'OW1', 'Z', 'W', 'D', 'AH1', 'B', 'EH1', 'V', 'IH1', 'AA1', 'R', 'AY1', 'ER0', 'AE1', 'AE2', 'AO1', 'NG', 'G', 'IH0', 'TH', 'IY2', 'F', 'DH', 'IY1', 'HH', 'UH1', 'IY0', 'OY1', 'OW2', 'CH', 'UW1', 'IH2', 'EH0', 'AO2', 'AA0', 'AA2', 'OW0', 'EY0', 'AE0', 'AW2', 'AW1', 'EY2', 'UW0', 'AH2', 'UW2', 'AO0', 'JH', 'Y', 'ZH', 'AY2', 'ER1', 'UH2', 'AY0', 'ER2', 'OY0', 'UH0', 'AW0', 'br', 'cg', 'lg', 'ls', 'ns', 'sil', 'sp']
        self.MONO_dict, self.MONO_dict_inv = self.get_monoph_dict()
        
        for file in file_list:
            file_vid=file.replace('/MEAD_3dmm','/MEAD-PH')
            file_vid=file_vid.replace('.mat','.json')
            file_text=file.replace('/MEAD_3dmm','/MEAD-subs')
            file_text=file_text.replace('.mat','.ass')
            if not os.path.isfile(file_text) or not os.path.isfile(file_vid):
                continue
            if 'M005' in file_vid or 'W015' in file_vid:
                continue 
            self.data.append([file,file_vid,file_text])



    def get_monoph_dict(self):
        Dict = {}
        Dict_inv = {}
        for i, ph in enumerate(self.MONOPHONE):
            Dict[ph] = i+1
            Dict_inv[str(i+1)] = ph
        return Dict, Dict_inv

    def phoneme2token(self,phoneme):
        tokens = []
        for ph in phoneme:
            tokens.append(self.MONO_dict[ph])

        return tokens

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        pose_path, ph_path, text_path = self.data[idx]
        with open(ph_path) as f:
            phoneme_alignments = json.load(f)
        phoneme_line = []
        for ph in phoneme_alignments:
            dur = ph[2] - ph[1]
            nb_ph = round(dur / self.UNIT_VALUE)
            if nb_ph==0:
                nb_ph=1
            phoneme_line = phoneme_line + [ph[0]] * nb_ph
        tokens = self.phoneme2token(phoneme_line)
        dmm = loadmat(pose_path)
        dmm= dmm['coeff']
        #angles = dmm[:, 224:227]
        #translations = dmm[:, 254:257]
        #pose = np.concatenate((angles,translations),axis=1)
        pose= np.transpose(dmm)
        text_parsed = parser.parse(text_path)
        text=''
        for t in text_parsed:
            text=text+t.text+' '
        token_tensor = torch.tensor(tokens,dtype=torch.int)
        pose_tensor = torch.tensor(pose, dtype=torch.float32)
        return token_tensor,text, pose_tensor


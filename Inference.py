import os

os.environ['CUDA_LAUNCH_BLOCKING'] = "1"
import argparse
import torch
import gaussian_diffusion as gf
import numpy as np
import Diffusion_model_para as tr_model
from scipy.io import loadmat,savemat
import json
from pysubparser import parser


def get_monoph_dict():
    MONOPHONE = ['EH2', 'K', 'S', 'L', 'AH0', 'M', 'EY1', 'SH', 'N', 'P', 'OY2', 'T', 'OW1', 'Z', 'W', 'D', 'AH1', 'B', 'EH1', 'V', 'IH1', 'AA1', 'R', 'AY1', 'ER0', 'AE1', 'AE2', 'AO1', 'NG', 'G', 'IH0', 'TH', 'IY2', 'F', 'DH', 'IY1', 'HH', 'UH1', 'IY0', 'OY1', 'OW2', 'CH', 'UW1', 'IH2', 'EH0', 'AO2', 'AA0', 'AA2', 'OW0', 'EY0', 'AE0', 'AW2', 'AW1', 'EY2', 'UW0', 'AH2', 'UW2', 'AO0', 'JH', 'Y', 'ZH', 'AY2', 'ER1', 'UH2', 'AY0', 'ER2', 'OY0', 'UH0', 'AW0', 'br', 'cg', 'lg', 'ls', 'ns', 'sil', 'sp']
    Dict = {}
    Dict_inv = {}
    for i, ph in enumerate(MONOPHONE):
        Dict[ph] = i + 1
        Dict_inv[str(i + 1)] = ph
    return Dict, Dict_inv


def phoneme2token(phoneme,MONO_dict):
    tokens = []
    for ph in phoneme:
        tokens.append(MONO_dict[ph])

    return tokens

def get_data():
    ph_path = 'input/audio.json'
    text_path = 'input/text.txt'
    dmm_path = 'input/3dmm.mat'
    pic_path = 'input/face.png'

    MONO_dict, MONO_dict_inv = get_monoph_dict()
    UNIT_VALUE =0.033
    with open(ph_path) as f:
        phoneme_alignments = json.load(f)
    phoneme_line = []
    for ph in phoneme_alignments:
        dur = ph[2] - ph[1]
        nb_ph = round(dur / UNIT_VALUE)
        phoneme_line = phoneme_line + [ph[0]] * nb_ph
    tokens = phoneme2token(phoneme_line,MONO_dict)
    dmm = loadmat(dmm_path)
    dmm= dmm['coeff']
    #angles = dmm[:, 224:227]
    #translations = dmm[:, 254:257]
    #pose = np.concatenate((angles,translations),axis=1)
    pose= np.transpose(dmm)
    with open(text_path) as f:
        text=f.readline()
    text=[text[:-2],text[:-2]]
    print(text)
    token_tensor = torch.tensor(tokens,dtype=torch.int)
    pose_tensor = torch.tensor(pose, dtype=torch.float32)

    lengths_audio = token_tensor.shape[-1]
    #print(lengths_audio)
    lengths_audio_t = torch.tensor(token_tensor.shape[-1])
    lengths_pose =pose_tensor.shape[-1]
    lengths_pose_t=torch.tensor(lengths_pose)
    lengths_audio =[lengths_audio,lengths_audio]
    audio_pad = torch.nn.functional.pad(token_tensor, (0,torch.max(lengths_audio_t)-token_tensor.shape[-1]))
    pose_pad = torch.nn.functional.pad(pose_tensor, (0, torch.max(lengths_pose_t)-pose_tensor.shape[-1]))
    
    audio_pad=torch.stack([audio_pad,audio_pad]).to('cuda')
    pose_pad=torch.permute(torch.stack([pose_pad,pose_pad]),(0,2,1)).to('cuda')
    mask_audio = (audio_pad!=0).to('cuda')

    mask_pose = (pose_pad != 0)
    mask_pose=mask_pose.sum(dim=-1)/pose_pad.shape[-1]
    mask_pose.to('cuda')

    return (audio_pad,text,pose_pad,mask_audio,mask_pose,lengths_audio,dmm_path,phoneme_line,pic_path)

def save_result(video_path,phoneme,dmm_path,generated_pose):

    ph_idx = {"AA": 0, "AE": 1, "AH": 2, "AO": 3, "AW": 4, "AY": 5, "B": 6, "CH": 7, "D": 8, "DH": 9, "EH": 10, "ER": 11, "EY": 12, "F": 13, "G": 14, "HH": 15, "IH": 16, "IY": 17, "JH": 18, "K": 19, "L": 20, "M": 21, "N": 22, "NG": 23, "NSN": 24, "OW": 25, "OY": 26, "P": 27, "R": 28, "S": 29, "SH": 30, "SIL": 31, "T": 32, "TH": 33, "UH": 34, "UW": 35, "V": 36, "W": 37, "Y": 38, "Z": 39, "ZH": 40}

    os.system("rm -r results")
    os.system("mkdir results")
    
    ph_token=[]
    for ph in phoneme:
        if len(ph)>2:
            ph=ph[:2]
        if ph in ph_idx:
            ph_token.append(ph_idx[ph])
        else:
            pht='SIL'
            ph_token.append(ph_idx[pht])
    with open('results/ph.json', "w") as fp:
        json.dump(ph_token, fp)

    os.system("cp " + dmm_path + " " + str(os.path.join('results', 'real.mat')))

    dmm = loadmat(dmm_path)
    coeff = dmm['coeff']
    nb_frames=len(phoneme)

    coeff = generated_pose[0,:nb_frames,:]#np.repeat(np.expand_dims(coeff[0,:],0),nb_frames,axis=0)

    #coeff[:, 224:227]=generated_pose[0,:nb_frames,:3]
    #coeff[:, 254:257]=generated_pose[0,:nb_frames,3:]
    transform=dmm['transform_params']
    #print(transform.shape)
    transform = np.repeat(np.expand_dims(transform[0,:],0),nb_frames,axis=0)
    #print(transform.shape)
    savemat(
        os.path.join('results', 'generated.mat'),
        {'coeff': coeff, 'transform_params': transform})


def Infer(data,model, diffusion, opt):
    model.load_state_dict(torch.load(f'{opt.load_weights}/model_weights_50'), strict=True)
    model.eval()
    audio,text,pose,mask_audio,mask_pose,len_pose,dmm_path,phoneme_line,vid_path=data
    B, T = audio.shape[:2]
    cur_len = torch.LongTensor([min(T, m_len) for m_len in len_pose])
    
    #print(text)
    
    xf_proj_audio, xf_out_audio = model.encode_audio(audio)
    xf_proj_text, xf_out_text = model.encode_text(text,"cuda")
    xf_proj_pose, xf_out_pose = model.encode_pose(pose[:,0,:],T)
    head_poses = diffusion.p_sample_loop(
        model,
        (B, T, opt.pose_size),
        clip_denoised=False,
        progress=True,
        model_kwargs={'xf_proj_audio': xf_proj_audio, 'xf_out_audio': xf_out_audio,'xf_proj_text':xf_proj_text, 'xf_out_text':xf_out_text,'xf_proj_pose':xf_proj_pose,'xf_out_pose':xf_out_pose, 'length': cur_len})

    torch.cuda.empty_cache()
    #head_poses = torch.permute(head_poses, (2, 0, 1))*mask_pose
    #head_poses = torch.permute(head_poses, (1,2,0))
    head_poses_np=head_poses.cpu().detach().numpy()

    save_result(vid_path, phoneme_line, dmm_path, head_poses_np)
    print('Done')


def main():
    parser = argparse.ArgumentParser()
    #parser.add_argument('-vid_path', type=str, default='')
    parser.add_argument('-epochs', type=int, default=25)
    parser.add_argument('-dropout', type=int, default=0.1)
    parser.add_argument('-batch_size', type=int, default=2)
    parser.add_argument('-audio_len', type=int, default=600)
    parser.add_argument('-audio_size', type=int, default=20)
    parser.add_argument('-audio_channels', type=int, default=768)
    parser.add_argument('-audio_rate', type=int, default=48000)
    parser.add_argument('-noise_size',type=int,default=500)
    parser.add_argument('-pose_len', type=int, default=500)
    parser.add_argument('-pose_size', type=int, default=257)
    parser.add_argument('-log_interval',type=int,default=2)
    parser.add_argument('-device', type=str, default='cuda')
    parser.add_argument('-printevery', type=int, default=1)
    parser.add_argument('-lr', type=int, default=0.0001)
    parser.add_argument('-num_worker', type=int, default=0)
    parser.add_argument('-load_weights', required=True)
    parser.add_argument('-diffusion_steps', type=int, default=1000)
    parser.add_argument('-save_every', type=int, default=1)

    opt = parser.parse_args()

    model = tr_model.MotionTransformer(257, num_frames=opt.audio_len)
    model = model.cuda()

    data=get_data()
    if data is None:
        print("Error")
        exit()

    beta_scheduler = 'linear'
    betas = gf.get_named_beta_schedule(beta_scheduler, opt.diffusion_steps)
    diffusion = gf.GaussianDiffusion(
        betas=betas,
        model_mean_type=gf.ModelMeanType.EPSILON,
        model_var_type=gf.ModelVarType.FIXED_SMALL,
        loss_type=gf.LossType.MSE
    )  # base is epsilon

    Infer(data,model, diffusion, opt)

if __name__ == "__main__":
    main()


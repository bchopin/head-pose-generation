import glob
import os
from p2fa_py3.p2fa import align_from_subs,align_new
import json
from tqdm import tqdm
from pysubparser import parser
import Levenshtein
from datetime import datetime


def main():
    file = 'input/audio.wav'
    json_file='input/audio.json'
    phoneme_alignments, _, _, _ = align_new.align(file, 'input/text.txt', state_align=True)
    with open(json_file, "w") as fp:
        json.dump(phoneme_alignments, fp)
    print('Done')

if __name__ == "__main__":
    main()

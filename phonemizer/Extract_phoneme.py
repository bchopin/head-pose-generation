import glob
import os
from p2fa_py3.p2fa import align_from_subs,align_new
import json
from tqdm import tqdm
from pysubparser import parser
import Levenshtein
from datetime import datetime


def get_MEAD_list(path):
    sentences=[]
    with open(path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if line == '':
                continue
            else:
                sentence = line[2:-1].upper()
                sentence = sentence.replace('’','\'')
                sentences.append(sentence)
    return sentences



def check_MEAD_string(path,sentences):
    text_parsed = parser.parse(path)
    text = ''
    for t in text_parsed:
        text = text + t.text + ' '
    text=text[:-1]
    for pun in [',', '.', ':', ';', '!', '?', '"', '%', '(', ')', '--', '---']:
        text = text.replace(pun, '')
    text = text.upper()
    if text in sentences:
        realsentence=text
    else:
        prob=0.0
        prob_idx=-1
        for i,sentence in enumerate(sentences):
            p= Levenshtein.ratio(text, sentence)
            if p>prob:
                prob=p
                prob_idx=i
        if prob_idx==-1:
            realsentence= None
        else:
            realsentence=sentences[prob_idx]
    return realsentence





def main():
    sentences = get_MEAD_list('/home/bchopin/Head_pose_diff_ph/phonemizer/MEAD_sentences')
    file_list = glob.glob('/data/stars/share/MEAD-subs/videos/**/*.ass',recursive=True)
    for file_text in tqdm(file_list):
        file = file_text.replace('/MEAD-subs', '/MEAD')
        file = file.replace('.ass', '.mp4')
        json_file=file.replace('/MEAD', '/MEAD-PH')
        json_file = json_file.replace('.mp4', '.json')
        if not os.path.isfile(file_text):
            print(f'no subs : {file}')
            continue
        if os.path.isfile(json_file):
            print(f'json already exists : {file}')
            continue
        if 'MEAD' in file or 'debug' in file:
            real_sentence=check_MEAD_string(file_text,sentences)
            if real_sentence is None:
                continue
            with open('/home/bchopin/Head_pose_diff_ph/phonemizer/tmptxt', 'w') as f:
                f.write(real_sentence)
            phoneme_alignments, _, _, _ = align_new.align(file,'/home/bchopin/Head_pose_diff_ph/phonemizer/tmptxt',state_align=True)
        else:
            phoneme_alignments, _, _, _ = align_from_subs.align(file, file_text, state_align=True)
        dir  = json_file.split('/')
        dir = '/'.join(dir[0:-1])
        os.makedirs(dir, exist_ok=True)
        with open(json_file, "w") as fp:
            json.dump(phoneme_alignments, fp)
    print('Done')

if __name__ == "__main__":
    main()
